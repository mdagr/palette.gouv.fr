
-   [{palette.gouv.fr}](#palettegouvfr)
    -   [Installation](#installation)
    -   [Utilisation](#utilisation)
    -   [Préparation](#préparation)
    -   [Exemples](#exemples)
        -   [Récupérer les codes
            hexadécimaux](#récupérer-les-codes-hexadécimaux)
        -   [Afficher des palettes](#afficher-des-palettes)
    -   [Qualités de couleurs de la gamme de base
        (gamme 1)](#qualités-de-couleurs-de-la-gamme-de-base-gamme-1)
    -   [Proposition de palettes qualitatives adaptées à la
        dyschromatopsie](#proposition-de-palettes-qualitatives-adaptées-à-la-dyschromatopsie)
        -   [Une palette
            semi-qualitative](#une-palette-semi-qualitative)
        -   [Une palette qualitative](#une-palette-qualitative)
    -   [Utilisation avec ggplot](#utilisation-avec-ggplot)
        -   [Couleurs simples](#couleurs-simples)
        -   [Palettes et gradients](#palettes-et-gradients)
    -   [Références](#références)

<!-- README.md is generated from README.Rmd. Please edit that file -->

# {palette.gouv.fr}

<!-- badges: start -->
<!-- badges: end -->

Utilisation des couleurs définies dans la charte graphique de l’État
français avec R.

## Installation

La version de développement de {palette.gouv.fr} peut s’installer depuis
Gitlab avec :

``` r
remotes::install_gitlab("mdagr/palette.gouv.fr")
```

ou en téléchargeant l’[archive
courante](https://gitlab.com/mdagr/palette.gouv.fr/-/archive/master/palette.gouv.fr-master.tar.gz)
et en l’installant avec :

``` r
install.packages("chemin/vers/archive/palette.gouv.fr-master.tar.gz", repos = NULL, type = "source")
```

## Utilisation

Le but du package
[{palette.gouv.fr}](https://gitlab.com/mdagr/palette.gouv.fr "Télécharger")
est de faciliter l’utilisation dans [R](https://www.r-project.org/) des
couleurs définies dans la charte graphique de l’État français ([Service
d’information du gouvernement,
2020](#ref-servicedinformationdugouvernementCharteGraphiqueEtat2020)).

La palette contient 93 couleurs à partir de 18 couleurs de base
(gamme 1), nommées de *A1* à *R1*, déclinées en 4 niveaux de luminosité
supplémentaires : une plus foncée (gamme 0, *A0* à *Q0*) et trois plus
claires (gammes 2 à 4, *A2*-*R2*, *etc.*) ; à cela sont ajoutées les
couleurs du drapeau tricolore.

![Palette originale](vignettes/img/source.png "Palette originale")

Certaines teintes ne sont pas présentes, ce qui limite les choix
possibles et rend certaines couleurs très proches. Il semble qu’il n’y a
pas vraiment eu de prise en compte des déficiences visuels et la
création d’une palette adaptée à la dyschromatopsie n’est pas évidente.
La création de dégradés ayant une bonne perception est aussi à examiner.

## Préparation

Il est conseillé d’installer en parallèle la police Marianne dans votre
système d’exploitation pour la typo des graphiques.

``` r
library(palette.gouv.fr)
library(extrafont)
library(purrr)
library(dplyr)
library(patchwork)
library(colorspace)
library(ggplot2)
```

## Exemples

L’ensemble des couleurs est disponible dans le data.frame
`couleurs_gouv_fr` :

``` r
head(couleurs_gouv_fr)
#> # A tibble: 6 x 7
#>   lettre gamme hex     nom       h     c     l
#>   <chr>  <chr> <chr>   <fct> <dbl> <dbl> <dbl>
#> 1 a      1     #958B62 a1     74.1  31.2  57.8
#> 2 a      0     #837C61 a0     74.2  21.2  52.0
#> 3 a      2     #CEC39C a2     73.3  31.0  78.8
#> 4 a      3     #ECE1BA a3     73.6  31.1  89.5
#> 5 a      4     #FEF3CC a4     73.7  31.1  95.8
#> 6 b      1     #91AE4F b1    103.   59.7  67.2
```

Une bibliothèque de différentes palettes basées sur ces couleurs est
aussi disponible dans la liste `biblio_gouv_fr` :

``` r
names(biblio_gouv_fr)
#>  [1] "gamme_0"              "gamme_1"              "gamme_2"             
#>  [4] "gamme_3"              "gamme_4"              "gamme_drapeau"       
#>  [7] "semi_dyschromatopsie" "dyschromatopsie"      "froid"               
#> [10] "chaud"                "melange"              "gris"
```

Et une fonction `palette_gouv_fr()` est proposée pour générer ou
afficher des palettes personnalisées. Il est aussi possible d’utiliser
des `scale_colour_gouv_fr()` et `scale_fill_gouv_fr()` avec {ggplot2}.

### Récupérer les codes hexadécimaux

Récupérer la palette de base (gamme 1) sous forme d’un vecteur nommé :

``` r
palette_gouv_fr() # ou biblio_gouv_fr$gamme_1
#>        a1        b1        c1        d1        e1        f1        g1        h1 
#> "#958B62" "#91AE4F" "#169B62" "#466964" "#00AC8C" "#5770BE" "#484D7A" "#FF8D7E" 
#>        i1        j1        k1        l1        m1        n1        o1        p1 
#> "#D08A77" "#FFC29E" "#FFE800" "#FDCF41" "#FF9940" "#E18B63" "#FF6F4C" "#7D4E5B" 
#>        q1        r1 
#> "#A26859" "#000000"
```

Récupérer seulement la couleur *A1* :

``` r
palette_gouv_fr("a1")
#>        a1 
#> "#958B62"
```

NB : le code des couleurs est insensible à la casse (on peut utiliser
*A1* ou *a1*)

Récupérer les couleurs *A1* et *C1* :

``` r
palette_gouv_fr("a1", "c1")
#>        a1        c1 
#> "#958B62" "#169B62"

# on n'est pas obligé de préciser la gamme si on veut la gamme par défaut (1)
palette_gouv_fr("a", "c")
#>        a1        c1 
#> "#958B62" "#169B62"
```

Récupérer toute la gamme 2 :

``` r
palette_gouv_fr(g = "2") # ou biblio_gouv_fr$gamme_2
#>        a2        b2        c2        d2        e2        f2        g2        h2 
#> "#CEC39C" "#BEDB82" "#6DD69D" "#94B4AF" "#65DFBE" "#A0B4FF" "#9CA0CC" "#FFC9C3" 
#>        i2        j2        k2        l2        m2        n2        o2        p2 
#> "#FFBCAC" "#FFE1D2" "#FFF3B6" "#FFE7B7" "#FFCDB5" "#FFC1AA" "#FFBCB3" "#CD9DA9" 
#>        q2        r2 
#> "#E7AA9C" "#777777"
```

Récupérer les couleurs *A2* et *C2* :

``` r
palette_gouv_fr("a2", "c2") 
#>        a2        c2 
#> "#CEC39C" "#6DD69D"
palette_gouv_fr("a", "c", g = "2")
#>        a2        c2 
#> "#CEC39C" "#6DD69D"
```

On peut mixer les gammes et spécifier l’ordre :

``` r
palette_gouv_fr("a1", "c3")
#>        a1        c3 
#> "#958B62" "#8EF5BB"
palette_gouv_fr("c3", "a1")
#>        c3        a1 
#> "#8EF5BB" "#958B62"
```

La gamme *drapeau* est particulière, elle ne contient que trois
couleurs : bleu france, blanc, rouge marianne.

``` r
palette_gouv_fr(g = "drapeau")  # ou biblio_gouv_fr$gamme_drapeau
#>    bleu france          blanc rouge marianne 
#>      "#000091"      "#FFFFFF"      "#E1000F"
palette_gouv_fr("bleu france")
#> bleu france 
#>   "#000091"
```

### Afficher des palettes

En précisant le format de sortie, on peut afficher les couleurs sous
forme de vecteur coloré
[{prismatic}](https://github.com/EmilHvitfeldt/prismatic) dans la
console ou sous forme de graphique
[{ggplot}](https://ggplot2.tidyverse.org/), avec leur code hexadécimal.

``` r
palette_gouv_fr(format = "prismatic")
#> <colors>
#> #958B62FF #91AE4FFF #169B62FF #466964FF #00AC8CFF #5770BEFF #484D7AFF #FF8D7EFF #D08A77FF #FFC29EFF #FFE800FF #FDCF41FF #FF9940FF #E18B63FF #FF6F4CFF #7D4E5BFF #A26859FF #000000FF
```

La gamme par défaut :

``` r
palette_gouv_fr(format = "plot")
```

<img src="man/figures/README-unnamed-chunk-12-1.png" width="100%" />

D’autres gammes :

``` r
palette_gouv_fr(g = "2", format = "plot")
```

<img src="man/figures/README-unnamed-chunk-13-1.png" width="100%" />

``` r
palette_gouv_fr(g = "drapeau", format = "plot")
```

<img src="man/figures/README-unnamed-chunk-13-2.png" width="100%" />

Des sélections (l’ordre est pris en compte) :

``` r
palette_gouv_fr("a1", "c1", format = "plot")
```

<img src="man/figures/README-unnamed-chunk-14-1.png" width="100%" />

``` r
palette_gouv_fr("k2", "c1", format = "plot")
```

<img src="man/figures/README-unnamed-chunk-14-2.png" width="100%" />

Toutes les couleurs :

``` r
unique(couleurs_gouv_fr$gamme) %>% 
  sort() %>% 
  purrr::map(~ palette_gouv_fr(g = ., format = "plot")) %>% 
  patchwork::wrap_plots(ncol = 2, nrow = 5, byrow = FALSE)
```

<img src="man/figures/README-unnamed-chunk-15-1.png" width="100%" />

## Qualités de couleurs de la gamme de base (gamme 1)

Le daltonisme n’a pas été pris en compte dans le choix des couleurs
initiales :

``` r
colorspace::swatchplot(palette_gouv_fr(), cvd = TRUE)
```

<img src="man/figures/README-unnamed-chunk-16-1.png" width="100%" />

Ainsi plusieurs couleurs sont plutôt indiscernables, comme par exemple
les couleurs *A*, *I*, *N* et *Q* pour les deutéranopes :

``` r
colorspace::swatchplot(palette_gouv_fr("a", "i", "n", "q"), cvd = TRUE)
```

<img src="man/figures/README-unnamed-chunk-17-1.png" width="100%" />

De même la luminosité varie trop irrégulièrement pour en faire une
palette continue efficace.

``` r
colorspace::specplot(palette_gouv_fr())
```

<img src="man/figures/README-unnamed-chunk-18-1.png" width="100%" />

``` r
colorspace::hclplot(palette_gouv_fr())
#> Warning in colorspace::hclplot(palette_gouv_fr()): cannot approximate L well as
#> a linear function of H and C
```

<img src="man/figures/README-unnamed-chunk-18-2.png" width="100%" />

Surtout quand on compare à Viridis par exemple :

``` r
colorspace::specplot(sequential_hcl(18, "Viridis"))
```

<img src="man/figures/README-unnamed-chunk-19-1.png" width="100%" />

``` r
colorspace::hclplot(sequential_hcl(18, "Viridis"))
```

<img src="man/figures/README-unnamed-chunk-19-2.png" width="100%" />

## Proposition de palettes qualitatives adaptées à la dyschromatopsie

### Une palette semi-qualitative

Elle n’est pas vraiment qualitative car elle n’a pas une luminosité
constante mais permet un choix de teinte plus large et permet la
reproduction en noir et blanc.

``` r
semi_qualitative_dyschromatopsie <- palette_gouv_fr("k", "b", "a", "f", "d", "g", "r")
colorspace::swatchplot(semi_qualitative_dyschromatopsie, cvd = TRUE)
```

<img src="man/figures/README-unnamed-chunk-20-1.png" width="100%" />

``` r
colorspace::specplot(semi_qualitative_dyschromatopsie)
```

<img src="man/figures/README-unnamed-chunk-20-2.png" width="100%" />

``` r
colorspace::hclplot(semi_qualitative_dyschromatopsie)
#> Warning in colorspace::hclplot(semi_qualitative_dyschromatopsie): cannot
#> approximate H well as a linear function of C and L
```

<img src="man/figures/README-unnamed-chunk-20-3.png" width="100%" />

### Une palette qualitative

On sélectionne les couleurs aux alentours de 70 % de luminosité :

``` r
couleurs_gouv_fr %>% 
  dplyr::filter(l > 65, l < 75) %>%
  dplyr::pull(nom) %>% 
  as.character() %>% 
  palette_gouv_fr(format = "plot")
```

<img src="man/figures/README-unnamed-chunk-21-1.png" width="100%" />

``` r
couleurs_gouv_fr %>% 
  dplyr::filter(l > 65, l < 75) %>%
  dplyr::pull(nom) %>% 
  as.character() %>% 
  palette_gouv_fr() %>% 
  colorspace::swatchplot(cvd = TRUE)
```

<img src="man/figures/README-unnamed-chunk-21-2.png" width="100%" />

Parmi ces couleurs, on peut par exemple en extraire cinq qui se
détachent assez bien :

``` r
qualitative_dyschromatopsie <- palette_gouv_fr("b1", "d2", "f2", "g2", "h1")
colorspace::swatchplot(qualitative_dyschromatopsie, cvd = TRUE)
```

<img src="man/figures/README-unnamed-chunk-22-1.png" width="100%" />

``` r
colorspace::specplot(qualitative_dyschromatopsie)
```

<img src="man/figures/README-unnamed-chunk-22-2.png" width="100%" />

``` r
colorspace::hclplot(qualitative_dyschromatopsie)
```

<img src="man/figures/README-unnamed-chunk-22-3.png" width="100%" />

Ces palettes sont utilisables directement avec
`biblio_gouv_fr$semi_dyschromatopsie` et
`biblio_gouv_fr$dyschromatopsie` ou dans les `scale_*()` (voir
ci-dessous).

## Utilisation avec ggplot

### Couleurs simples

``` r
ggplot2::ggplot(mtcars, ggplot2::aes(hp, mpg)) +
ggplot2::geom_point(colour = palette_gouv_fr("e1"),
           size = 3, alpha = 1)
```

<img src="man/figures/README-unnamed-chunk-23-1.png" width="100%" />

### Palettes et gradients

``` r
ggplot2::ggplot(iris, ggplot2::aes(Sepal.Width, Sepal.Length, colour = Species)) +
ggplot2::geom_point(size = 3) +
ggplot2::scale_colour_manual(
  values = palette_gouv_fr("a", "j", "k", 
  noms = c("setosa", "versicolor", "virginica")))
```

<img src="man/figures/README-unnamed-chunk-24-1.png" width="100%" />

On peut utiliser les palettes définies dans `biblio_gouv_fr` :

``` r
ggplot2::ggplot(iris, ggplot2::aes(Sepal.Width, Sepal.Length, colour = Species)) +
ggplot2::geom_point(size = 3) +
scale_colour_gouv_fr("dyschromatopsie")
```

<img src="man/figures/README-unnamed-chunk-25-1.png" width="100%" />

``` r
ggplot2::diamonds %>% 
  dplyr::sample_frac(0.05) %>% 
  ggplot2::ggplot(ggplot2::aes(carat, price, colour = cut)) +
  ggplot2::geom_point(size = 3) +
  scale_colour_gouv_fr(palette = "gamme_1")
```

<img src="man/figures/README-unnamed-chunk-26-1.png" width="100%" />

## Références

<div id="refs" class="references csl-bib-body hanging-indent"
entry-spacing="1">

<div id="ref-servicedinformationdugouvernementCharteGraphiqueEtat2020"
class="csl-entry">

Service d’information du gouvernement. *Charte graphique de l’État*.
\[s.l.\] : République française, 2020. Disponible sur :
&lt; <https://www.gouvernement.fr/marque-Etat> &gt;

</div>

</div>
